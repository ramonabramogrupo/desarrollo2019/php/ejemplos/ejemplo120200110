<?php

$numeros=[
    1,4,5,6,7,8,9
];

//imprimir los numeros del array
//for
for($c=0;$c<count($numeros);$c++){
?>
<li><?= $c . ": " . $numeros[$c] ?></li>
<?php    
}
for($c=0;$c<count($numeros);$c++){
    echo "<li>$c: $numeros[$c]</li>";
}
//while
$c=0;
while($c<count($numeros)){
    echo "<li>$c: $numeros[$c]</li>";
    $c++;
}
//foreach
foreach ($numeros as $indice=>$valor){
    echo "<li>$indice: $valor</li>";
}

